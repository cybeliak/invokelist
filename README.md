InvokeList
======

A slightly verified and modified version of the word-frequency list found on:

https://invokeit.wordpress.com/frequency-word-lists/

These lists are released under Creative Commons – Attribution / ShareAlike 3.0 licens, which can be found at:

http://creativecommons.org/licenses/by-sa/3.0/
